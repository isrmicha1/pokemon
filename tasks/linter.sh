#!/bin/bash
GITDIFFFILES=`git diff --name-only --cached --diff-filter=ACMRTUXB | grep ".*\.js$" | tr "\n" " "`
echo "Running eslint on files $GITDIFFFILES"
yarn eslint  --ext .js $GITDIFFFILES --fix