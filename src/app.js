import React from 'react'
import ReactDOM from 'react-dom'
import { Home } from './containers/home'
import Store from './store'
import { Provider } from 'react-redux'
ReactDOM.render(
  <Provider store={Store}>
    <Home />
  </Provider>,
  document.getElementById('app')
)
